package com.example.lok.notifications_61_62;

import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

/**
 * Created by Lok on 03/08/2017.
 *
 * Code partially from https://stackoverflow.com/questions/33495294/testing-notifications-in-android
 *
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentationTest {

    @Test
    public void testNotification() throws UiObjectNotFoundException {
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        device.pressHome();

        // Bring up the default launcher by searching for a UI component
        // that matches the content description for the launcher button.
        UiObject allAppsButton = device.findObject(new UiSelector().description("Apps"));
        allAppsButton.clickAndWaitForNewWindow();

        // Next, in the apps tabs, we can simulate a user swiping until
        // they come to the Settings app icon. Since the container view
        // is scrollable, we can use a UiScrollable object.
        UiScrollable appViews = new UiScrollable(
                new UiSelector().scrollable(true));

        //Scroll down till "(61-62)Notifications" app is in view
        appViews.scrollTextIntoView("(61-62)Notifications");

        //Get reference to this App
        UiObject notificationApp = device.findObject(new UiSelector().description("(61-62)Notifications"));
        // Perform a click on the button to launch the app.
        notificationApp.click();

        //Get reference to the button
        UiObject2 notificationButton = device.findObject(By.text("Show Notification"));
        // Perform a click on the button to launch the notification.
        notificationButton.click();

        //Checks if notification exists
        device.openNotification();
        device.wait(Until.hasObject(By.text("Here is the title")), System.currentTimeMillis());
        UiObject2 title = device.findObject(By.text("Here is the title"));
        UiObject2 text = device.findObject(By.text("This is the body text of your notification"));
        assertEquals(("Here is the title"), title.getText());
        assertEquals(("This is the body text of your notification"), text.getText());
        title.click();

        //Check if Activity is shown again by verifying text on button
        onView(withId(R.id.button)).check(matches(withText(R.string.show_notification)));
    }
}