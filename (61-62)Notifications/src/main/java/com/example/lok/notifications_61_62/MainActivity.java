package com.example.lok.notifications_61_62;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;

/*
Allows the user to click a Button whereupon a Notification is being built and shown, when the user
clicks on the notification he will be brought back to the app. First the Notification is
instantiated with NotificationCompat.Builder. Then after clicking the Button showButton() is
called where the state of the Notification object is set. Next a PendingIntent is being instantiated
with another Intent among other variables and attached to the PendingIntent object with
setContentIntent(). After instantiating NotificationManager it's instance method notify() is being
called to post the notification to be shown in the status bar.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 61 - Notifications
From https://www.youtube.com/watch?v=SWsuijO5NGE&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=61
Android App Development for Beginners - 62 - Custom Notifications
From https://www.youtube.com/watch?v=NgQzJ0s0XmM&index=62&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class MainActivity extends AppCompatActivity {

    //FIELDS
    private NotificationCompat.Builder notification;
    private static final int uniqueID = 45678;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);//remove after the notification is clicked

    }

    public void showButton(View view) {
        //Build Notification
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker(getString(R.string.ticker));
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle(getString(R.string.title));
        notification.setContentText(getString(R.string.body_text));

        //Give the phone the ability to handle the intent (Add PendinIntent for when user clicks on the notification)
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        //Build the Notification and issue it
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());
    }
}
