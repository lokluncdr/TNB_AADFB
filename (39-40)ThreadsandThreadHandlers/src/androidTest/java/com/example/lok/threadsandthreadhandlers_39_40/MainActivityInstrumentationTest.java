package com.example.lok.threadsandthreadhandlers_39_40;

import android.support.test.espresso.IdlingResource;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.IdlingRegistry.getInstance;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Lok on 02/08/2017.
 *
 * Inspired by https://www.youtube.com/watch?v=uCtzH0Rz5XU
 * https://developer.android.com/reference/android/support/test/espresso/idling/CountingIdlingResource.html
 * https://developer.android.com/topic/libraries/testing-support-library/release-notes.html
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentationTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private IdlingResource mIdlingResource;

    @Before
    public void registerIdlingResource() {
        //Register the IdlingResoucre from MainActivity for synchronization
        mIdlingResource = mainActivityActivityTestRule.getActivity().countingIdlingResource;
        // To prove that the test fails, omit this call:
        getInstance().register(mIdlingResource);
    }

    @Test
    public void testTextView() {
        //Verify default text
        onView(withId(R.id.myTextView)).check(matches(withText(R.string.hello)));

        //Perform action
        onView(withId(R.id.button)).perform(click());

        //Check if TextView has changed
        onView(withId(R.id.myTextView)).check(matches(withText(R.string.very_good_man)));
    }
}