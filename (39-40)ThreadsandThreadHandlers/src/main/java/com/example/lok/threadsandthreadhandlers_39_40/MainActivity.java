package com.example.lok.threadsandthreadhandlers_39_40;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/*
Allows the user to create a thread and start it when the user clicks a button. In the onClick() a
timer is started that waits for 10 seconds before changing a text "Hello" in a TextView to
"Very Good Man!". The waiting happens in a Runnable which is passed on to a Thread on instantiation
of the latter. However in order to change the user interface from a Thread, it's best to use a
Handler object which was made specific earlier (with an anonymous inner class during object
instantiation of Handler where handleMessage() is overridden.) by calling sendEmptyMessage().

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 39 - Threads
From https://www.youtube.com/watch?v=1TiEQ0WhvSU&index=39&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 40 - Thread Handlers
From https://www.youtube.com/watch?v=SCwU-gy3HoM&index=40&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */
public class MainActivity extends AppCompatActivity {

    //FIELDS
    public CountingIdlingResource countingIdlingResource = new CountingIdlingResource("DATA_LOADER"); //For Testing Purposes
                                                        //Ensures the test waits till a thread is done doing something
                                                        //Just increment it before the thread is started and decrement it
                                                        //after it is finished

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            TextView myTextView = (TextView) findViewById(R.id.myTextView);
            myTextView.setText(R.string.very_good_man);
        }
    };

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long futureTime = System.currentTimeMillis() + 10000;
                while (System.currentTimeMillis() < futureTime) {
                    synchronized (this) {

                        try {
                            wait(futureTime - System.currentTimeMillis());
                        } catch (Exception e) {

                        }
                    }
                }
                //Calls the handler
                handler.sendEmptyMessage(0);
                countingIdlingResource.decrement();
            }
        };
        countingIdlingResource.increment();

        Thread thread = new Thread(runnable);
        thread.start();
    }
}
