package com.example.lok.gestures_21_22;

import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.TextView;

import com.example.lok.runningthegestureapp2122.R;

/*
Allows the user to see which gestures are being made by himself.
Shows only a TextView. On different gestures one of 9 different texts are being shown. MainActivity
implements GestureDetector.OnGestureListener and GestureDetector.OnDoubleTapListener. OnGestureListener
is an Interface of class GestureDetector which forces you to implement onDown(), onShowPress(), onSingleTapUp(),
onScroll(), onLongPress() and onFling(); OnDoubleTapListener is an Interface of class GestureDetector which forces
you to implement onSingleTapConfirmed(), onDoubleTap() and onDoubleTapEvent(). Lastly onTouchEvent() is implemented
in MainActivity to handle touch screen motion events.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 21 - Gestures
From https://www.youtube.com/watch?v=zsNpiOihNXU&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=21
Android App Development for Beginners - 22 - Running the Gesture App
From https://www.youtube.com/watch?v=V_KbEfhf1qc&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=22
 */
public class MainActivity extends AppCompatActivity implements GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {

    //FIELDS
    private TextView changingText;
    public GestureDetectorCompat gestureDetector;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //        fab.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View view) {
        //                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //                        .setAction("Action", null).show();
        //            }
        //        });

        changingText = (TextView) findViewById(R.id.placeholder_text);
        this.gestureDetector = new GestureDetectorCompat(this, this);
        gestureDetector.setOnDoubleTapListener(this);

    }

    ////// Begin Gestures //////
    //Implemented methods from GestureDetector.OnDoubleTapListener
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        changingText.setText(R.string.onSingleTapConfirmed);
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        changingText.setText(R.string.onDoubleTap);
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        changingText.setText(R.string.onDoubleTapEvent);
        return true;
    }

    //Implemented methods from GestureDetector.OnGestureListener
    @Override
    public boolean onDown(MotionEvent e) {
        changingText.setText(R.string.onDown);
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        changingText.setText(R.string.onShowPress);

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        changingText.setText(R.string.onSingleTapUp);
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        changingText.setText(R.string.onScroll);
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        changingText.setText(R.string.onLongPress);

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        changingText.setText(R.string.onFling);
        return true;
    }

    ////// End Gestures //////

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.gestureDetector.onTouchEvent(event); //true if the GestureDetector.OnGestureListener consumed the event, else false.
        return super.onTouchEvent(event);
    }


    //    @Override
    //    public boolean onCreateOptionsMenu(Menu menu) {
    //        // Inflate the menu; this adds items to the action bar if it is present.
    //        getMenuInflater().inflate(R.menu.menu_main, menu);
    //        return true;
    //    }
    //
    //    @Override
    //    public boolean onOptionsItemSelected(MenuItem item) {
    //        // Handle action bar item clicks here. The action bar will
    //        // automatically handle clicks on the Home/Up button, so long
    //        // as you specify a parent activity in AndroidManifest.xml.
    //        int id = item.getItemId();
    //
    //        //noinspection SimplifiableIfStatement
    //        if (id == R.id.action_settings) {
    //            return true;
    //        }
    //
    //        return super.onOptionsItemSelected(item);
    //    }
}
