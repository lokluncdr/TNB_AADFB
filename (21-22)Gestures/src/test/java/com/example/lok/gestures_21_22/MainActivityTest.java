package com.example.lok.gestures_21_22;

import android.os.SystemClock;
import android.support.v4.view.GestureDetectorCompat;
import android.view.MotionEvent;
import android.widget.TextView;

import com.example.lok.runningthegestureapp2122.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Created by Lok on 23/07/2017.
 */
//@Config(constants = BuildConfig.class)
@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {

    private MainActivity activity;

    // @Before => JUnit 4 annotation that specifies this method should run before each test is run
    // Useful to do setup for objects that are needed in the test
    @Before
    public void setup() {
        // Convenience method to run MainActivity through the Activity Lifecycle methods:
        // onCreate(...) => onStart() => onPostCreate(...) => onResume()
        //        activity = Robolectric.setupActivity(MainActivity.class);
        activity = Robolectric.buildActivity(MainActivity.class).create().get();

        //Verify Text is "Whatever"
        TextView defaultText = (TextView) activity.findViewById(R.id.placeholder_text);
        String defaultTextValue = defaultText.getText().toString();
        assertNotNull("TextView could not be found", defaultTextValue);
        assertThat(defaultTextValue, equalTo(activity.getString(R.string.placeholder_text)));
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull(activity);
    }

    ////// Begin Gestures //////
    //Implemented methods from GestureDetector.OnDoubleTapListener
    @Test
    public void onSingleTapConfirmed() throws Exception {
        //Do on single tap confirmed
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onSingleTapConfirmed(motionEvent);

        //Verify Text is "onSingleTapConfirmed"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onSingleTapConfirmed)));
    }

    @Test
    public void onDoubleTap() throws Exception {
        //Do double tap
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onDoubleTap(motionEvent);

        //Verify Text is "onDoubleTap"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onDoubleTap)));
    }

    @Test
    public void onDoubleTapEvent() throws Exception {
        //Do double tap event
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onDoubleTapEvent(motionEvent);

        //Verify Text is "onDoubleTapEvent"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onDoubleTapEvent)));
    }

    //Implemented methods from GestureDetector.OnGestureListener
    @Test
    public void onDown() throws Exception {
        //Do on down
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onDown(motionEvent);

        //Verify Text is "onDown"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onDown)));
    }

    @Test
    public void onShowPress() throws Exception {
        //Do on show press
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onShowPress(motionEvent);

        //Verify Text is "onShowPress"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onShowPress)));
    }

    @Test
    public void onSingleTapUp() throws Exception {
        //Do on single tap up
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onSingleTapUp(motionEvent);

        //Verify Text is "onSingleTapUp"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onSingleTapUp)));
    }

    @Test
    public void onScroll() throws Exception {
        //Do on scroll
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        MotionEvent motionEvent2 = motionEvent;
        Float x = 20.f;
        Float y = 20.f;
        activity.onScroll(motionEvent, motionEvent2, x, y);

        //Verify Text is "onScroll"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onScroll)));
    }

    @Test
    public void onLongPress() throws Exception {
        //Do on long press
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        activity.onLongPress(motionEvent);

        //Verify Text is "onLongPress"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onLongPress)));
    }

    @Test
    public void onFling() throws Exception {
        //Do on fling
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        MotionEvent motionEvent2 = motionEvent;
        Float x = 20.f;
        Float y = 20.f;
        activity.onFling(motionEvent, motionEvent2, x, y);

        //Verify Text is "onFling"
        TextView postText = (TextView) activity.findViewById(R.id.placeholder_text);
        String postTextValue = postText.getText().toString();
        assertNotNull("TextView could not be found", postTextValue);
        assertThat(postTextValue, equalTo(activity.getText(R.string.onFling)));
    }

    ////// End Gestures //////
    @Test
    public void onTouchEvent() throws Exception {
        //Purpose = running the onTouchEvent() on MainActivity with custom MotionEvents

        GestureDetectorCompat gestureDetectorCompat = activity.gestureDetector;

        //Set up the MotionEvent with parameter ACTION_DOWN, because it's an OnTouchEvent
        long downTime = SystemClock.uptimeMillis();
        long eventTime = SystemClock.uptimeMillis();
        eventTime += 10;
        MotionEvent motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_DOWN, 1, 1, 0);
        //Check if MotionEvent really is onTouchEvent
        assertTrue("GestureDetector.OnGestureListener didn't consume the event", gestureDetectorCompat.onTouchEvent(motionEvent));//onTouchEvent() returns true if the GestureDetector.OnGestureListener consumed the event, else false.

        //Set up the MotionEvent with parameter ACTION_SCROLL, because it's not an onTouchEvent
        motionEvent = MotionEvent.obtain(downTime, eventTime, MotionEvent.ACTION_SCROLL, 1, 1, 0); //About ACTION_SCROLL ->
        // This action is not a touch event so it is delivered to onGenericMotionEvent(MotionEvent) rather than onTouchEvent(MotionEvent).
        assertFalse("GestureDetector.OnGestureListener did consume the event", gestureDetectorCompat.onTouchEvent(motionEvent));//onTouchEvent() returns true if the GestureDetector.OnGestureListener consumed the event, else false.
    }
}