package com.example.lok.sqlite_49_54;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by Lok on 03/08/2017.
 *
 * Inspired by http://www.singhajit.com/testing-android-database/
 *
 */
@RunWith(AndroidJUnit4.class)
public class MyDBHandlerInstumentationTest {

    private MyDBHandler database;

    //Test Variables for DB
    private String product1 = "Monitor";
    private String product2 = "Water";
    private String product3 = "Coffee";

    @Before
    public void setUp() throws Exception {
        getTargetContext().deleteDatabase(MyDBHandler.TABLE_NAME);

        Context context = InstrumentationRegistry.getTargetContext();
        String name = MyDBHandler.TABLE_NAME;
        int version = MyDBHandler.DATABASE_VERSION;

        database = new MyDBHandler(context, name, null, version);

        //Clear DB from old data by deleting it
        database.truncate(name);
    }

    @After
    public void tearDown() throws Exception {
        database.close();
    }

    @Test
    public void shouldAddProduct() throws Exception {
        //Add 1 product
        database.addProduct(new Product(product1));
        List<String> products = database.getProducts();
        assertThat(products.size(), is(1));
        assertTrue(products.get(0).equals(product1));

        //Add 2nd product
        database.addProduct(new Product(product2));
        products = database.getProducts();
        assertThat(products.size(), is(2));
        assertTrue(products.get(0).equals(product1));
        assertTrue(products.get(1).equals(product2));

        //Add 3rd product
        database.addProduct(new Product(product3));
        products = database.getProducts();
        assertThat(products.size(), is(3));
        assertTrue(products.get(0).equals(product1));
        assertTrue(products.get(1).equals(product2));
        assertTrue(products.get(2).equals(product3));
    }

    @Test
    public void shouldDeleteProduct() throws Exception
    {
        //Add 3 products
        database.addProduct(new Product(product1));
        database.addProduct(new Product(product2));
        database.addProduct(new Product(product3));

        //Delete product 1
        database.deleteProduct(product1);
        List<String> products = database.getProducts();
        assertThat(products.size(), is(2));
        assertTrue(products.get(0).equals(product2));
        assertTrue(products.get(1).equals(product3));

        //Delete product 2
        database.deleteProduct(product2);
        products = database.getProducts();
        assertThat(products.size(), is(1));
        assertTrue(products.get(0).equals(product3));

        //Delete product 3
        database.deleteProduct(product3);
        products = database.getProducts();
        assertThat(products.size(), is(0));
    }
}