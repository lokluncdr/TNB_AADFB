package com.example.lok.sqlite_49_54;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/*

Allows user to type a text into an EditText, after clicking the "Add" button the text will be
displayed in a TextView and it's value will be saved into the SQLite database, afterwards the
same value can be removed from TextView and from the database by typing it into the EditText
and by clicking the "Delete" button. First of all a Custom SQLiteOpenHelper class is being made
called MyDBHandler (Which must implement onCreate() and onUpgrade()). In onCreate() the query is
set up for creation of the database in a String and executed with execSQL() on the database object.
This is all part of the instantiation of the MyDBHandler class which happens in MainActivity's
onCreate().  Furthermore instance methods addProduct(), deleteProduct() and databaseToString()
are being used in MainActivity as well where they are called in the buttonclick methods. Lastly
Product pojo was also made to fill column "_productname" in table "products" in database
"products.db".

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 49 - Saving Data with SQLite
From https://www.youtube.com/watch?v=_vQaOvPsLko&index=49&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 50 - SQLite Database Product
From https://www.youtube.com/watch?v=q3rhteIierY&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=50
Android App Development for Beginners - 51 - Creating a New SQLite Database
From https://www.youtube.com/watch?v=Jcmp09LkU-I&index=51&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 52 - Adding and Deleting Rows
From https://www.youtube.com/watch?v=GAyvtK4cWLA&index=52&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 53 - Printing Database Results
From https://www.youtube.com/watch?v=bTWJ8d1hbvw&index=53&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 54 - Testing the SQLite App
From https://www.youtube.com/watch?v=PrMhjEh2ySk&index=54&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class MainActivity extends AppCompatActivity {

    //FIELDS
    EditText myInput;
    TextView myTextView;
    MyDBHandler myDBHandler;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myInput = (EditText)findViewById(R.id.myInput);
        myTextView = (TextView) findViewById(R.id.showTextView);
        myDBHandler = new MyDBHandler(this, null, null, 1);

        printDatabase();
    }

    private void printDatabase() {
        String dbString = myDBHandler.databaseToString();
        myTextView.setText(dbString);
        myInput.setText("");
    }

    public void addButtonClick(View view) {
        Product product = new Product(myInput.getText().toString());
        myDBHandler.addProduct(product);
        printDatabase();
    }

    public void deleteButtonClick(View view) {
        String inputText  = myInput.getText().toString();
        myDBHandler.deleteProduct(inputText);
        printDatabase();
    }
}
