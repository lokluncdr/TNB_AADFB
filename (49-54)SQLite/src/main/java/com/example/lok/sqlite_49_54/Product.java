package com.example.lok.sqlite_49_54;

/**
 * Created by Lok on 11/04/16.
 */
public class Product {

    //FIELDS
    private int _id;
    private String _productname;

    //CONSTRUCTOR
    public Product() {
    }

    public Product(String productname) {
        this._productname = productname;
    }

    //METHODS
    public int get_id() {
        return _id;
    }

    public String get_productname() {
        return _productname;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_productname(String _productname) {
        this._productname = _productname;
    }

}
