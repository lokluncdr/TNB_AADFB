package com.example.lok.listview_45_46;

import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ListView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Lok on 27/07/2017.
 * <p>
 * Inspired by  http://www.singhajit.com/instrumentation-testing-of-listview/
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class MainActivityInstrumentationTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    private String[] foods = {"Bacon", "Chips", "Chickenwings", "Pretzels", "IceCream", "Snickers"};
    private String currentFood;

    /**
     * Check that the item is created. onData() takes care of scrolling.
     */
    @Test
    public void checkListViewContent() {

        final MainActivity ma = mainActivityActivityTestRule.getActivity();

        //Check amount of items in ListView is correct
        final ListView listView = (ListView) ma.findViewById(R.id.myListView);
        assertThat("ListView's amount isn't correct", listView.getCount(), is(6));

        //Check if String values in ListView are correct
        for (int i = 0; i < foods.length; i++) {
            assertThat("ListView's items aren't correct", (String) listView.getItemAtPosition(i), is(foods[i]));
        }
    }
}
