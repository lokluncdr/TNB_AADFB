package com.example.lok.listview_45_46;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

/*
Allows the user to see a ListView with mock objects and click on a row (which is then confirmed by a
Toast). All is set up in MainActivity's onCreate(). Array of String objects is first created, which
is then passed into object instantiation of ArrayAdapter which is then passes as the adapter for
the ListView object with setAdapter() after instantiating the object of ListView with the correct
View. Lastly the user click is being handled by onItemClick() of AdapterView.OnItemClickListener
in setOnItemClickListener(0 of ListView object.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 45 - ListView
From https://www.youtube.com/watch?v=U_Jvk4G28YE&index=45&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 46 - ListView Example
From https://www.youtube.com/watch?v=A-_hKWMA7mk&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=46
 */

public class MainActivity extends AppCompatActivity {

    private String[] foods = {"Bacon", "Chips", "Chickenwings", "Pretzels", "IceCream", "Snickers"};
    private ListAdapter myAdapter;
    private ListView myListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, foods);
        myListView = (ListView) findViewById(R.id.myListView);
        myListView.setAdapter(myAdapter);

        myListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String food = String.valueOf(parent.getItemAtPosition(position));

                        Toast.makeText(MainActivity.this, food, Toast.LENGTH_LONG).show();
                    }
                });
    }
}
