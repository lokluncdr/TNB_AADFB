package com.example.lok.videoplayer_55;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

/*
Allows the user to start a certain video which plays its content from the web. First a VideoView
object must be declared in activity_main.xml which will then be used for reference on the VideoView
instantiation in MainActivity's onCreate(). Next the video path is set with setVideoPath(). Then a
MediaController is instantiated so that it can be passed on to the  method setMediaController() of
VideoView's object (To add Play, pause, stop functionality to the video).  Lastly video is started
with start().

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 55 - Playing Video
From https://www.youtube.com/watch?v=oF9yZenJtjI&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=55
 */

public class MainActivity extends AppCompatActivity {

    //FIELDS




    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final VideoView videoView = (VideoView)findViewById(R.id.myVideoView);
        //From url
        videoView.setVideoPath("http://www.webestools.com/page/media/videoTag/BigBuckBunny.mp4");

        //From /raw folder
//        String uri = "android.resource://" + getPackageName() + "/" + R.raw.samplevideo_1280x720_2mb;
//        videoView.setVideoURI(Uri.parse(uri));

        //Videoplayer controls (Play, pause, stop, )
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        videoView.start();
    }
}
