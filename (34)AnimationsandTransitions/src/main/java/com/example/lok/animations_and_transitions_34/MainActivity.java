package com.example.lok.animations_and_transitions_34;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/*
Allows the user to touch the background which causes a Button to be moved from upper left corner to
bottom right corner while it also changes in size. First on the ViewGroup of the layout (standard
activity_main.xml) onTouch() is being overridden in setOnTouchListener(). Next to change the position
of the button there are rules which need to be added with a call to addRule() on
RelativeLayout.LayoutParams object. Lastly the instance variables: width and height of the
ViewGroup.LayoutParams object (instantiated with layout parameters of the button) are assigned a
different value.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 34 - Animations and Transitions
From: https://www.youtube.com/watch?v=n4IyvL-ACbk&index=34&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
*/

public class MainActivity extends AppCompatActivity {

    ViewGroup myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myLayout = (ViewGroup) findViewById(R.id.myLayout);
        myLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        moveButton();

                        return false;
                    }
                });
    }

    private void moveButton() {
        View myButton = (View) findViewById(R.id.myButton);

        TransitionManager.beginDelayedTransition(myLayout);

        //Change position of button
        RelativeLayout.LayoutParams positionRules = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        positionRules.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        positionRules.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        if (myButton != null) {
            myButton.setLayoutParams(positionRules);
        }

        //Change size of button
        ViewGroup.LayoutParams sizeRules = null;
        if (myButton != null) {
            sizeRules = myButton.getLayoutParams();
        }
        sizeRules.width = 450;
        sizeRules.height = 300;

        myButton.setLayoutParams(sizeRules);
    }
}
