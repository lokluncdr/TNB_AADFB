package com.example.lok.overflow_menu_33;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Lok on 25/07/2017.
 * <p>
 * https://stackoverflow.com/questions/33965723/espresso-click-menu-item
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityInstrumentationTest {

    //For testing the colors
    private final static String RED = "Red";
    private final static String YELLOW = "Yellow";
    private final static String BLUE = "Blue";
    private Context context;
    private int viewById;

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    //Since the background may not be a color, you can use linearLayout.getBackground() which will give you a Drawable.
    // There is no API to get background color, specifically.
    //From https://stackoverflow.com/questions/14779259/get-background-color-of-a-layout
    public static int getBackgroundColor(View view) {
        Drawable drawable = view.getBackground();
        if (drawable instanceof ColorDrawable) {
            ColorDrawable colorDrawable = (ColorDrawable) drawable;
            if (Build.VERSION.SDK_INT >= 11) {
                return colorDrawable.getColor();
            }
            try {
                Field field = colorDrawable.getClass().getDeclaredField("mState");
                field.setAccessible(true);
                Object object = field.get(colorDrawable);
                field = object.getClass().getDeclaredField("mUseColor");
                field.setAccessible(true);
                return field.getInt(object);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Before
    public void setupContext() {
        //For using the application's context
        context = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void onOptionsItemRedSelected() throws Exception {

        //TEST RED
        //Click first item in OverFlowMenu
        openActionBarOverflowOrOptionsMenu(context);
        //Use withText as withId doesn't work
        onView(withText(RED)).perform(click());

        //Get reference to the right layout and verify if background is the correct color
        //        int viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().main_view);
        viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().findViewById(R.id.main_view));
        assertThat(viewById, is(Color.RED));
    }

    @Test
    public void onOptionsItemYellowSelected() throws Exception {

        //TEST YELLOw
        //Click first item in OverFlowMenu
        openActionBarOverflowOrOptionsMenu(context);
        //Use withText as withId doesn't work
        onView(withText(YELLOW)).perform(click());

        //Get reference to the right layout and verify if background is the correct color
        //        int viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().main_view);
        viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().findViewById(R.id.main_view));
        assertThat(viewById, is(Color.YELLOW));
    }

    @Test
    public void onOptionsItemBlueSelected() throws Exception {
        //TEST BLUE
        //Click first item in OverFlowMenu
        openActionBarOverflowOrOptionsMenu(context);
        //Use withText as withId doesn't work
        onView(withText(BLUE)).perform(click());

        //Get reference to the right layout and verify if background is the correct color
        //        int viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().main_view);
        viewById = getBackgroundColor(mainActivityActivityTestRule.getActivity().findViewById(R.id.main_view));
        assertThat(viewById, is(Color.BLUE));
    }
}