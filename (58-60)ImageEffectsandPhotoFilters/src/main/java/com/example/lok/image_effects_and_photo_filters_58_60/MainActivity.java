package com.example.lok.image_effects_and_photo_filters_58_60;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    //FIELDS
    private ImageView imageView;
    private Drawable buckysFace;
    private Bitmap bitmapImage;

    private Bitmap photoOriginal;
    private Bitmap photoEffect;
    private LayerDrawable layerEffect;

    private ImageView imageView2;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Invert Bitmap image
        imageView = (ImageView) findViewById(R.id.imageView);
        buckysFace = getResources().getDrawable(R.drawable.bucky, null);
        bitmapImage = ((BitmapDrawable) buckysFace).getBitmap();

        Bitmap newPhoto = invertImage(bitmapImage);
        imageView.setImageBitmap(newPhoto);


        //Show image with filter
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        Drawable photoSource = ContextCompat.getDrawable(this, R.drawable.bucky);
        photoOriginal = ((BitmapDrawable) photoSource).getBitmap(); //Convert (png, jpg...) image to bitmap and store it in memory.
        imageView2.setImageBitmap(photoOriginal);

        addFilter();
    }


    public static Bitmap invertImage(Bitmap original) {
        Bitmap finalImage = Bitmap.createBitmap(original.getWidth(), original.getHeight(), original.getConfig());

        int A, R, G, B;
        int pixelColor;
        int height = original.getHeight();
        int width = original.getWidth();

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {

                pixelColor = original.getPixel(x, y);
                A = Color.alpha(pixelColor);
                R = 255 - Color.red(pixelColor);
                G = 255 - Color.green(pixelColor);
                B = 255 - Color.blue(pixelColor);

                finalImage.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        return finalImage;
    }

    public void addFilter() {
        //Check if filtered photo exists, and just show it in that case.
        if (photoEffect != null) {
            imageView.setImageBitmap(photoEffect);
            return;
        }

        photoEffect = Bitmap.createBitmap(photoOriginal.getWidth(), photoOriginal.getHeight(), photoOriginal.getConfig());

        Drawable[] layers = new Drawable[2];
        layers[0] = ContextCompat.getDrawable(this, R.drawable.bucky);
        layers[1] = ContextCompat.getDrawable(this, R.drawable.filter);
        layerEffect = new LayerDrawable(layers);
        layerEffect.setBounds(0, 0, photoOriginal.getWidth(), photoOriginal.getHeight());
        layerEffect.draw(new Canvas(photoEffect));

        imageView2.setImageBitmap(photoEffect);
    }
}
