package com.example.lok.customlistview_47_48;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Lok on 11/04/16.
 *
 * Allows the user to see a custom ListView with mock objects and click on a row (which is then
 * confirmed by a Toast). All is set up in MainActivity's onCreate(). Array of String objects is
 * first created, which is then passed into object instantiation of a custom ArrayAdapter which
 * is then passed as the adapter for the ListView object with setAdapter() after instantiating
 * the object of ListView with the correct View.  The difference between regular ListView and
 * CustomListView is that class CustomAdapter (Which extends ArrayAdapter) uses it's own xml
 * file specifically designed for each row in the ListView (which is being used for setting
 * the ImageView and TextView). Lastly the user click is being handled by onItemClick() of
 * AdapterView.OnItemClickListener in setOnItemClickListener() of ListView object.
 *
 * TNB, AADFB (The New Boston, Android App Development For Beginners)
 *
 * Android App Development for Beginners - 47 - Custom ListView Row
 * From https://www.youtube.com/watch?v=_sStCBdJkQg&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=47
 * Android App Development for Beginners - 48 - Custom ListView Adapter
 * From https://www.youtube.com/watch?v=nOdSARCVYic&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=48
 *
 */
public class CustomAdapter extends ArrayAdapter {

    //FIELDS

    //CONSTRUCTOR
    public CustomAdapter(Context context, String[] foods) {
        super(context, R.layout.custom_row, foods);
    }

    //METHODS

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        return super.getView(position, convertView, parent);
        LayoutInflater myInflater = LayoutInflater.from(getContext()); //getContext() means Backgroundinformation
        View customView = myInflater.inflate(R.layout.custom_row, parent, false);

        String singleFoodItem = (String) getItem(position);
        TextView myText = (TextView)customView.findViewById(R.id.myTextView);
        ImageView myImageView = (ImageView)customView.findViewById(R.id.myImageView);

        myText.setText(singleFoodItem);
        myImageView.setImageResource(R.mipmap.bucky);

        return customView;
    }
}
