package com.example.lok.styles_63;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/*
Allows user to open the app, see 3 TextViews with static text in 3 different ways with each of
the TextViews applying their own Style. The Style is applied in each TextView object in
activity_main.xml, either through its attributes or through attributes added with the reference
of attribute style="@style/BabyBlue" which refers to a style in the xml file ocean.xml.
Style BabyBlue is also "subclassed" in style BabyBlue.BigBlue.


TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 63 - Styles
From https://www.youtube.com/watch?v=S2V5jlLjwPU&index=63&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
