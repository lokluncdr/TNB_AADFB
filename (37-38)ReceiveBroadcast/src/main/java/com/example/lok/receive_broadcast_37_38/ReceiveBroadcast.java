package com.example.lok.receive_broadcast_37_38;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/*
ReceiveBroadcast
Allows the user to receive a Broadcast and send a Toast to notify user that Broadcast has been
received. Has no Activity and no App icon. In order to listen to the Broadcast that was received
and to target the specific Broadcast, an Intent Filter is created in the AndroidManifest.xml.

SendBroadcast
Allows the user to send a Broadcast. In order to do this a special Intent needs to be instantiated
(With no class name, but with a keyword (to recognize this Broadcast) for this Broadcast:
"com.example.lok.send_broadcast_37_38" added to sendIntent.setAction().) And the custom
ReceiveBroadcast extends BroadcastReceiver and must override onReceive() in which the Toast
is created and displayed. Lastly when the "app" is being runned, "Do not launch Activity" is
chosen.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 38 - Receiving Broadcast Intents
From https://www.youtube.com/watch?v=htU_Rd-DW2U&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=38
 */
public class ReceiveBroadcast extends BroadcastReceiver {
    public ReceiveBroadcast() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Broadcast has been received", Toast.LENGTH_LONG).show();
    }
}
