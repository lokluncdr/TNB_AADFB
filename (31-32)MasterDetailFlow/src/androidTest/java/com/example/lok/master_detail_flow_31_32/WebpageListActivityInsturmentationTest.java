package com.example.lok.master_detail_flow_31_32;

import android.content.Intent;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.web.webdriver.Locator;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.web.assertion.WebViewAssertions.webMatches;
import static android.support.test.espresso.web.sugar.Web.onWebView;
import static android.support.test.espresso.web.webdriver.DriverAtoms.findElement;
import static android.support.test.espresso.web.webdriver.DriverAtoms.getText;
import static org.hamcrest.Matchers.containsString;

/**
 * Created by Lok on 24/07/2017.
 * <p>
 * Inspired by https://github.com/googlesamples/android-testing/tree/master/ui/espresso/WebBasicSample
 * for xpath http://www.qaautomated.com/2016/02/how-to-locate-android-app-ui-elements.html
 * http://www.qaautomated.com/2016/02/testing-web-views-using-espresso-web.html
 * https://stackoverflow.com/questions/41680458/android-espresso-select-an-item-from-a-spinner-in-a-webview
 * https://android.googlesource.com/platform/frameworks/testing/+/android-support-test/espresso/sample/src/androidTest/java/android/support/test/testapp/WebViewTest.java
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class WebpageListActivityInsturmentationTest {

    private static final int DESIRED_ITEM_BUCKY = 0;  //Bucky
    private static final int DESIRED_ITEM_FORUM = 1;  //Forum
    private static final int DESIRED_ITEM_VIDEOS = 2;  //Videos
    private static final String KEY_URL_TO_LOAD = "KEY_URL_TO_LOAD";

    @Rule
    public ActivityTestRule<WebpageListActivity> mWebListActivity = new ActivityTestRule<>(WebpageListActivity.class);

    //Different ways for enabling Javascript
    //1.
    //    @Rule
    //    public ActivityTestRule<WebpageListActivity> mWebListActivity = new ActivityTestRule<WebpageListActivity>(WebpageListActivity.class, false, true) {
    //        @Override
    //        protected void afterActivityLaunched() {
    //            // Technically we do not need to do this - WebViewActivity has javascript turned on.
    //            // Other WebViews in your app may have javascript turned off, however since the only way
    //            // to automate WebViews is through javascript, it must be enabled.
    //            onWebView().forceJavascriptEnabled();
    //        }
    //    };
    //2.
    //    @Before
    //    public void setUp()
    //    {
    //         enable java script for web page to perform web interactions
    //
    //        onWebView().forceJavascriptEnabled();
    //    }

    /**
     * @param url
     * @return start {@link Intent} for the simple web form URL.
     * source: https://github.com/googlesamples/android-testing/blob/master/ui/espresso/WebBasicSample/app/src/androidTest/java/com/example/android/testing/espresso/web/BasicSample/WebViewActivityTest.java
     */
    private static Intent withWebFormIntent(String url) {
        Intent basicFormIntent = new Intent();
        basicFormIntent.putExtra(KEY_URL_TO_LOAD, url);
        return basicFormIntent;
    }

    @Test
    public void scrollToItem_CheckItsTextInWebviewBucky() {
        //Scroll to preferred position and click on it, so it navigates and tests if WebpageDetailActivity and it's corresponding
        //WebpageDetailFragment (Where WebView is being used) are called correctly
        onView(withId(R.id.webpage_list)).perform(RecyclerViewActions.actionOnItemAtPosition(DESIRED_ITEM_BUCKY, click()));

        //Verify if the text being diplayed is correct
        // Selects the WebView in your layout.
        onWebView().forceJavascriptEnabled() //Make sure to enable Javascript
                // Find the input element by ID
                .withElement(findElement(Locator.XPATH, "//*[@id=\"content-wrapper\"]/ul/li[2]")) /*Uses the xpath of
                                                                                                    <li class="active">
                                                                                                    <a href="https://thenewboston.com/profile.php?user=2">
                                                                                                    Bucky Roberts's  Profile
                                                                                                    </a>
                                                                                                    </li>*/
                //In Chrome -> go to website -> right click  -> Inspect -> Click item -> Right click -> Copy -> Copy XPath
                // Verify that the response page contains the entered text
                .check(webMatches(getText(), containsString("Bucky Roberts's Profile")));
    }

    @Test
    public void scrollToItem_CheckItsTextInWebviewForum() {
        //Scroll to preferred position and click on it, so it navigates and tests if WebpageDetailActivity and it's corresponding
        //WebpageDetailFragment (Where WebView is being used) are called correctly
        onView(withId(R.id.webpage_list)).perform(RecyclerViewActions.actionOnItemAtPosition(DESIRED_ITEM_FORUM, click()));

        //Verify if the text being diplayed is correct
        // Selects the WebView in your layout.
        onWebView().forceJavascriptEnabled() //Make sure to enable Javascript
                // Find the input element by ID
                .withElement(findElement(Locator.XPATH, "//*[@id=\"content-wrapper\"]/ul/li[1]"))   //Uses the xpath of
                // <li><a href="https://thenewboston.com/forum">Forum</a></li>
                //In Chrome -> go to website -> right click  -> Inspect -> Click item -> Right click -> Copy -> Copy XPath
                // Verify that the response page contains the entered text
                .check(webMatches(getText(), containsString("Forum")));
    }

    @Test
    public void scrollToItem_CheckItsTextInWebviewVideos() {
        //Scroll to preferred position and click on it, so it navigates and tests if WebpageDetailActivity and it's corresponding
        //WebpageDetailFragment (Where WebView is being used) are called correctly
        onView(withId(R.id.webpage_list)).perform(RecyclerViewActions.actionOnItemAtPosition(DESIRED_ITEM_VIDEOS, click()));

        //Verify if the text being diplayed is correct
        // Selects the WebView in your layout.
        onWebView().forceJavascriptEnabled() //3. Make sure to enable Javascript
                // Find the input element by ID
                .withElement(findElement(Locator.XPATH, "//*[@id=\"content-wrapper\"]/div[1]/div"))    //Uses the xpath of
                                                                                                        /*<div class="col-xs-12">
                                                                                                        <h1 class="margin-top0 featured-video-heading">Watch Thousands of Tutorials for Free!</h1>
                                                                                                        <p class="margin-bottom25">Welcome to the worlds largest collection of computer and technology related tutorials on the web. Feel free to browse through our library of over 7,000 videos and tutorials. We have courses in programming, web design, video editing, game development, and more. Join the over 180 million people who have already enjoyed the benefits of online learning.</p>
                                                                                                        </div>*/
                /***This methods doesn't recognize the real text written in HTML, because the string below in constainsString() is what seems
                 * to be send back by JS or something...? Otherwise the string would have been "Watch Thousands of Tutorials for Free!"
                 * Also not the best string, but the website lacks clarity with this page being for Videos w.r.t. a clear header... So I settled for this solution..***/
                //In Chrome -> go to website -> right click  -> Inspect -> Click item -> Right click -> Copy -> Copy XPath
                // Verify that the response page contains the entered text
                .check(webMatches(getText(), containsString("Featured Tutorial - Android App Development for Beginners")));
    }
}