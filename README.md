[Samples/]

Update 03-08-2017: Added Tests (Apps with "T." before the Application's name below(above the dotted line -------)). See bottom, below the dotted line ------- for more.



Followed TNB, AADFB (The New Boston, Android App Development For Beginners) on 
https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
and made 19-67.

T. (19-20)EventListenerandCallbackMethod
Android App Development for Beginners - 19 - Event Listener and Callback Method
From https://www.youtube.com/watch?v=jxoG_Y6dvU8&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=19
Android App Development for Beginners - 20 - Multiple Event Listeners
From https://www.youtube.com/watch?v=bBUeH5oNBxI&index=20&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (21-22)Gestures
Android App Development for Beginners - 21 - Gestures
From https://www.youtube.com/watch?v=zsNpiOihNXU&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=21
Android App Development for Beginners - 22 - Running the Gesture App
From https://www.youtube.com/watch?v=V_KbEfhf1qc&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=22

T. (23-30)Fragments
Android App Development for Beginners - 23 - Fragments
From https://www.youtube.com/watch?v=dQ6uc__qP-g&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=23
Android App Development for Beginners - 24 - Designing the Top Fragment
From https://www.youtube.com/watch?v=PWAlLiy5p2k&index=24&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 25 - Creating the Fragment Class
From https://www.youtube.com/watch?v=yOBQHf5nM2I&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=25
Android App Development for Beginners - 26 - Finishing the Meme Apps Design
From https://www.youtube.com/watch?v=dYy9adsIJDQ&index=26&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 27 - Listening for Button Clicks
From https://www.youtube.com/watch?v=vyykjIPNBXY&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=27
Android App Development for Beginners - 28 - Communicating with Main Activity
From https://www.youtube.com/watch?v=MHHXxWbSaho&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=28
Android App Development for Beginners - 29 - Changing the Memes Text
From https://www.youtube.com/watch?v=BzCj2-6jZKo&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=29
Android App Development for Beginners - 30 - Dank Meme Bro
From https://www.youtube.com/watch?v=sPvCEsGm8us&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=30

T. (31-32)MasterDetailFlow
Android App Development for Beginners - 31 - Master Detail Flow
From https://www.youtube.com/watch?v=jNoLvbD-O6E&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=31
Android App Development for Beginners - 32 - Master Detail Flow Example
From https://www.youtube.com/watch?v=iNzrUu784dY&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=32

T. (33)OverflowMenu
Android App Development for Beginners - 33 - Overflow Menu
From https://www.youtube.com/watch?v=iwE1bnRlZw0&index=33&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

(34)AnimationsandTransitions
Android App Development for Beginners - 34 - Animations and Transitions
From: https://www.youtube.com/watch?v=n4IyvL-ACbk&index=34&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (35-36,41,42)Intents,IntentService,Service
Android App Development for Beginners - 35 - Intents
From https://www.youtube.com/watch?v=06bcsMx-7QQ&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=35
Android App Development for Beginners - 36 - Sending Extra Intent Data
From https://www.youtube.com/watch?v=mPGCLKRCG-8&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=36
Android App Development for Beginners - 41 - Intent Service
From https://www.youtube.com/watch?v=-sBxmjrSn34&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=41
Android App Development for Beginners - 42 - Services
From https://www.youtube.com/watch?v=9YapLOfQ-dY&index=42&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (37-38)ReceiveBroadcast
Android App Development for Beginners - 38 - Receiving Broadcast Intents
From https://www.youtube.com/watch?v=htU_Rd-DW2U&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=38

(37-38)SendBroadcast
Android App Development for Beginners - 37 - Sending Broadcast Intents
From https://www.youtube.com/watch?v=X69q01TY1ic&index=37&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (39-40)ThreadsandThreadHandlers
Android App Development for Beginners - 39 - Threads
From https://www.youtube.com/watch?v=1TiEQ0WhvSU&index=39&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 40 - Thread Handlers
From https://www.youtube.com/watch?v=SCwU-gy3HoM&index=40&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (43-44)BoundServices
Android App Development for Beginners - 43 - Bound Services
From https://www.youtube.com/watch?v=0c4jRCm353c&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=43

T. (45-46)ListView
Android App Development for Beginners - 45 - ListView
From https://www.youtube.com/watch?v=U_Jvk4G28YE&index=45&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 46 - ListView Example
From https://www.youtube.com/watch?v=A-_hKWMA7mk&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=46

T. (47-48)CustomListView
Android App Development for Beginners - 47 - Custom ListView Row
From https://www.youtube.com/watch?v=_sStCBdJkQg&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=47
Android App Development for Beginners - 48 - Custom ListView Adapter
From https://www.youtube.com/watch?v=nOdSARCVYic&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=48

T. (49-54)SQLite
Android App Development for Beginners - 49 - Saving Data with SQLite
From https://www.youtube.com/watch?v=_vQaOvPsLko&index=49&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 50 - SQLite Database Product
From https://www.youtube.com/watch?v=q3rhteIierY&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=50
Android App Development for Beginners - 51 - Creating a New SQLite Database
From https://www.youtube.com/watch?v=Jcmp09LkU-I&index=51&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 52 - Adding and Deleting Rows
From https://www.youtube.com/watch?v=GAyvtK4cWLA&index=52&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 53 - Printing Database Results
From https://www.youtube.com/watch?v=bTWJ8d1hbvw&index=53&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 54 - Testing the SQLite App
From https://www.youtube.com/watch?v=PrMhjEh2ySk&index=54&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (55)VideoPlayer
Android App Development for Beginners - 55 - Playing Video
https://www.youtube.com/watch?v=oF9yZenJtjI&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=55

T. (56-57)Camera
Android App Development for Beginners - 56 - Image Capture
https://www.youtube.com/watch?v=my8PSy2DBsY&index=56&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 57 - Taking Photos with the Camera
https://www.youtube.com/watch?v=k1Wc0vmD284&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=57

(58-60)ImageEffectsandPhotoFilters

T. (61-62)Notifications
Android App Development for Beginners - 61 - Notifications
https://www.youtube.com/watch?v=SWsuijO5NGE&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=61
Android App Development for Beginners - 62 - Custom Notifications
https://www.youtube.com/watch?v=NgQzJ0s0XmM&index=62&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

(63)Styles
Android App Development for Beginners - 63 - Styles
https://www.youtube.com/watch?v=S2V5jlLjwPU&index=63&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

(64)Themes
Android App Development for Beginners - 64 - Themes
https://www.youtube.com/watch?v=3g-HKRJrtnI&index=64&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl

T. (65-66)SharedPreferences
Android App Development for Beginners - 65 - Shared Preferences
From https://www.youtube.com/watch?v=hCJkUd7YEXs&index=65&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 66 - Shared Preferences Example
From https://www.youtube.com/watch?v=xv_JJbjDQ3M&index=66&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxlFrom 

(67)CustomLauncherIcon

----------------------------------------------------------------------------------------------

19-20 EventListenerandCallBackMethod
/androidTest
Uses Espresso to test if TextView changes correctly on Button click()/longclick().

21-22 Gestures
/test
Uses Robolectric to test if TextViews change correctly on calling all (implemented) methods from GestureDetector.OnGestureListener and GestureDetector.OnDoubleTapListener. Also checks if MainActivity isn’t null and if onTouchEvent()’s gestureDetector.onTouchEvent() works. It mostly relies on Asserts(Checking if a certain condition is being met). Gestures are made with MotionEvent objects.

23-30 Fragments
/androidTest
Uses Espresso to test if 2 TextViews change correctly on Button click() after putting the texts in 2 EditTexts. Although TextViews are on another Fragment than the EditTexts, with onView(withId(R.id.blabla)) they’re easily retrieved.

31-32 MasterDetailFlow
/androidTest
Uses Espresso to test a RecyclerView on WebPageListActivity by clicking on every item (Bucky, Forum, Videos) and checking if the WebView(Called an WebPageDetailFragment, which in turn is called on WebpageDetailActivity) shows the right website by taking the XPath of a certain node in HTML and then checking if it contains a certain String value. Dependency androidTestCompile 'com.android.support.test.espresso:espresso-web:2.2.2’  in Gradle is especially important.

33 OverflowMenu
/androidTest
Uses Espresso to test if Overflowmenu Items Red, Yellow and Blue work as intended, afterwards this is being checked by Assertions to verify that the background color has changed correctly.

35-36,41,42 Intents,IntentService, Service
/androidTest
Intent
Uses Espresso to test if text from EditText on Water Activity is correctly passed on to the Beach Acitivty with an Intent when the user clicks on a button (for which dependendies: androidTestCompile 'com.android.support.test:runner:1.0.0’, androidTestCompile 'com.android.support.test:rules:1.0.0’, androidTestCompile 'com.android.support.test.espresso:espresso-core:3.0.0’, androidTestCompile and 'com.android.support.test.espresso:espresso-intents:3.0.0’ are used). Then Beach Activity is verified by checking the text shown in TextView (which was passed on along with the Intent) . Afterwards on button click the test goes back to Water Activity which is verified by checking the text displayed on TextView.
IntentService
According to https://developer.android.com/training/testing/integration-testing/service-testing.html Note: The ServiceTestRule class does not support testing of IntentService objects. If you need to test a IntentService object, you should encapsulate the logic in a separate class and create a corresponding unit test instead. Since this IntentService doesn't contain any logic, there's no real use for testing it in isolation with a Unit Test...
Service
Uses Espresso to test if the (unbound) Service is correctly started and serveral lifecycle methods are being tested.

37-38 ReceiveBroadcast
/test
Uses Robolectric to test if BroadcastReceiver correctly defined in the application (AndroidManifest.xml). Next it checks if the BroadcastReceiver is right for our specific Intent and verify if there’s only one intent.

39-40 ThreadsandThreadHandlers
/test
Uses Espresso to test if TextView is filled with the right text, however the text only changes after a certain amount of time is passed by in a Thread. For which an CountingIdlingResource is being used. Before the Thread starts the CountingIdlingResource object (in MainActivity) is being incremented, so that the test waits with further action, untill the CountingIdlingResource object is being decremented again. Do make sure to register the CountingIdlingResource object in the Test class with getInstance().register(mIdlingResource);.

43-44 BoundServices
/androidTest
Uses Espresso to test if the Bound Service is running correctly by calling it’s method and asserting that it’s returning value is of String.class.

45-46 ListView
/androidTest
Uses Espresso to test if amount of items in ListView are correct as well as their String values.

47-48 CustomListView
/androidTest
Uses Espresso to test if amount of items in ListView are correct as well as their String values.

49-54 SQLite
/androidTest
Uses Espresso to test if methods (from class MyDBHandler) for adding and deleting values from DB are handled correctly.

55 VideoPlayer
/androidTest
Used Espresso Test Recorder to click on the video and choose an assertion. It Merely checks if the VideoView exists (Which I assume isn’t the same as  checking if it’s being played). 

56-57 Camera
/androidTest
Uses Espresso and Espresso Intents to test if ImageView doesn’t have an image, then performs a Button click and afterwards checks if ImageView does have an image. Also the Intent is stubbed with fake data with @Before annotation. So we know what goes in the Intent and can check it’s result in a more controlled manner.

61-62 Notifications
/androidTest
Uses UI Automator to test if notification works properly and is set correctly. 

65-66 SharedPreferences
/test
Uses Mockito to unit test two methods. First whether saving and reading something from Shared preferences works correctly. Second that a broken MockEditor and a broken SharedPreferencesHelper are not returning any results.
