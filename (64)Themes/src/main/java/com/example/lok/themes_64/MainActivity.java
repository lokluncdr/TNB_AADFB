package com.example.lok.themes_64;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/*
Allows user to open the app, see 3 TextViews with static text and has a certain Background color.
The style applied in each TextView object in activity_main.xml comes from its attributes.
There is a Theme however that's being applied (Either to one Activity or the whole App) and it
sets the Background with <item name="android:windowBackground">@color/sky_color</item> derived
from the xml sky.xml. This is the Theme which is added AndroidManifest.xml under Activity:
<activity android:name=".MainActivity" as its attribute.


TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 64 - Themes
From https://www.youtube.com/watch?v=3g-HKRJrtnI&index=64&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
