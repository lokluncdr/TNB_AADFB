package com.example.lok.send_broadcast_37_38;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

/*
ReceiveBroadcast
Allows the user to receive a Broadcast and send a Toast to notify user that Broadcast has been
received. Has no Activity and no App icon. In order to listen to the Broadcast that was received
and to target the specific Broadcast, an Intent Filter is created in the AndroidManifest.xml.

SendBroadcast
Allows the user to send a Broadcast. In order to do this a special Intent needs to be instantiated
(With no class name, but with a keyword (to recognize this Broadcast) for this Broadcast:
"com.example.lok.send_broadcast_37_38" added to sendIntent.setAction().) And the custom
ReceiveBroadcast extends BroadcastReceiver and must override onReceive() in which the Toast
is created and displayed. Lastly when the "app" is being runned, "Do not launch Activity" is
chosen.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 37 - Sending Broadcast Intents
From https://www.youtube.com/watch?v=X69q01TY1ic&index=37&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class SendBroadcast extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_broadcast);
    }

    public void sendBroadcast(View view) {

        Intent sendIntent = new Intent();
        sendIntent.setAction("com.example.lok.send_broadcast_37_38");
        sendIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES); // To make this Broadcast available for all versions of Android
        sendBroadcast(sendIntent);
    }
}
