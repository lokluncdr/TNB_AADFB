package com.example.lok.camera_56_57;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/*
Allows the user to make a picture after clicking a Button and the picture from camera is then being
displayed in an ImageView. This is done by firing an Intent, for the capturing of an image with the
camera, with startActivityForResult(); Calling the last method ensures something is passed back to
onActivityResult() which has to be overridden. Here the picture is being retrieved from the Intent
passed as parameter and cast to a Bitmap object, after which it is set to the ImageView with
setImageBitmap().

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 56 - Image Capture
From https://www.youtube.com/watch?v=my8PSy2DBsY&index=56&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 57 - Taking Photos with the Camera
From https://www.youtube.com/watch?v=k1Wc0vmD284&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=57
 */

public class MainActivity extends AppCompatActivity {

    //FIELDS
    static final int REQUEST_IMAGE_CAPTURE = 1; //Identify your Intent (Open up camera)
    ImageView imageView;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        imageView = (ImageView) findViewById(R.id.imageView);

        //Disable the button if the user doesn't have a camera
        if (!hasCamera()) {
            button.setEnabled(false);
        }
    }

    private boolean hasCamera() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //Take picture and pass results to onActivityResult()
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    //If you want tor return the image taken

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);

        //Check if you actually do have an image, without errors
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            //Get photo
            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");
            imageView.setImageBitmap(photo);

        }
    }
}
