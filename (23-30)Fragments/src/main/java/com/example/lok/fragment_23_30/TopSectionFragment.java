package com.example.lok.fragment_23_30;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Lok on 08/04/16.
 */

public class TopSectionFragment extends Fragment {

    //FIELDS
    private static EditText topTextInput;
    private static EditText bottomTextInput;
    TopSectionListener activityCommander;

    //METHODS
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        activityCommander = (TopSectionListener) context;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_section_fragment, container, false);

        topTextInput = (EditText) view.findViewById(R.id.topTextInput);
        bottomTextInput = (EditText) view.findViewById(R.id.bottomTextInput);
        final Button button = (Button) view.findViewById(R.id.button_Meme_ify);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked(v);
            }
        });

        return view;
    }

    public void buttonClicked(View view) {
        activityCommander.createMeme(topTextInput.getText().toString(), bottomTextInput.getText().toString());
    }

    //InterfaceA
    public interface TopSectionListener {
        public void createMeme(String top, String bottom);
    }
}
