package com.example.lok.fragment_23_30;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

/*
Allows the user to create memes, based on a top text and a bottom text provided by the user. Shows a MainActivity with
two Fragments named: BottomPictureFragment and TopSectionFragment. BottomPictureFragment shows a default Image as background
and has two TextViews (Top and Bottom) with a default static text. TopSectionFragment has two EditTexts and a Button. When
the user clicks on the button the two texts provided by the user in the EditTexts (from TopSectionFragment) will be shown in
the TextViews (from the BottomPictureFragment). Both Fragments are added through XML in content_main.xml. They have their own
classes though, which extend from Fragment. MainActivity implements TopSectionFragment.TopSectionListener. TopSectionListener
is an Interface with method createMeme() in which BottomPictureFragment is declared, so that setMemeText() of BottomPictureFragment
can be called. Lastly to tie it all together onAttach() (which is called when Fragment is first attached to it's context) is overridden
in TopSectionFragment so that MainActivity's context can be used to instantiate TopSectionListener object.

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 23 - Fragments
From https://www.youtube.com/watch?v=dQ6uc__qP-g&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=23
Android App Development for Beginners - 24 - Designing the Top Fragment
From https://www.youtube.com/watch?v=PWAlLiy5p2k&index=24&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 25 - Creating the Fragment Class
From https://www.youtube.com/watch?v=yOBQHf5nM2I&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=25
Android App Development for Beginners - 26 - Finishing the Meme Apps Design
From https://www.youtube.com/watch?v=dYy9adsIJDQ&index=26&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 27 - Listening for Button Clicks
From https://www.youtube.com/watch?v=vyykjIPNBXY&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=27
Android App Development for Beginners - 28 - Communicating with Main Activity
From https://www.youtube.com/watch?v=MHHXxWbSaho&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=28
Android App Development for Beginners - 29 - Changing the Memes Text
From https://www.youtube.com/watch?v=BzCj2-6jZKo&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=29
Android App Development for Beginners - 30 - Dank Meme Bro
From https://www.youtube.com/watch?v=sPvCEsGm8us&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=30
*/

public class MainActivity extends AppCompatActivity implements TopSectionFragment.TopSectionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


    }

    //Gets called by the Top Fragment when user clicks the button. Pieces both fragments together and passes data from TopSectionFragment to BottomPictureFragment.
    @Override
    public void createMeme(String top, String bottom) {
            BottomPictureFragment bottomFragment = (BottomPictureFragment) getSupportFragmentManager().findFragmentById(R.id.fragment2);
            bottomFragment.setMemeText(top, bottom);
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
