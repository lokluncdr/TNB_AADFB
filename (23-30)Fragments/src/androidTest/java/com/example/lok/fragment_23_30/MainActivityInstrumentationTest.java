package com.example.lok.fragment_23_30;

import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Lok on 24/07/2017.
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class MainActivityInstrumentationTest {

    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     * <p>
     * <p>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void createMeme() throws Exception {
        //Check that Texts are empty strings "" first
        //Get reference to both EditText texts
        String textTop = mMainActivityTestRule.getActivity().getResources().getString(R.string.empty);
        String textBottom = mMainActivityTestRule.getActivity().getResources().getString(R.string.empty);
        onView(withId(R.id.topTextInput)).check(matches(withText(textTop)));
        onView(withId(R.id.bottomTextInput)).check(matches(withText(textBottom)));

        String topText = "Hoi";
        String bottomText = "Sjaak";

        //Get reference to both EditTexts
        //Enter text in top EditText
        onView(withId(R.id.topTextInput)).perform(typeText(topText), closeSoftKeyboard());
        //Enter text in bottom EditText
        onView(withId(R.id.bottomTextInput)).perform(typeText(bottomText), closeSoftKeyboard());

        //Get reference to Button and Perfom click on Button
        onView((withId(R.id.button_Meme_ify))).perform(click());

        //Get reference to top TextView and Verify if top TextViews' text is correct
        onView(withId(R.id.topMemeText)).check(matches(withText(topText)));

        //Get reference to bottom TextView and Verify if bottom TextViews' text is correct
        onView(withId(R.id.bottomMemeText)).check(matches(withText(bottomText)));
    }
}