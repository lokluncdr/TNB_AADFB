package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Created by Lok on 26/07/2017.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class WaterInstrumentationTest {

    @Rule
    public IntentsTestRule<Water> mWater = new IntentsTestRule<Water>(Water.class);

    private String message = "Sunny";
    @Test
    public void enterText_CheckNextActivity()
    {
        //Locate EditText and enter text
        onView(withId(R.id.editText)).perform(typeText(message), closeSoftKeyboard());

        //Locate button and click
        onView(withId(R.id.buttonGoToBeach)).perform(click());

        //Verify the intent
        intended(allOf(hasExtra("waterMessage",message)));

        //Verify text on TextView in BeachActivity
        onView(withId(R.id.beachTextView)).check(matches(withText(message)));

        //Go Back agin
        onView(withId(R.id.buttonGoToWater)).perform(click());

        //Verify that text on TextView in WaterActivity is correct
        onView(withId(R.id.textView_Water)).check(matches(withText(mWater.getActivity().getApplicationContext().getString(R.string.water))));
    }

}