package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by Lok on 26/07/2017.
 *
 *From https://medium.com/@josiassena/android-how-to-unit-test-a-service-67e5340544a5
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ServiceTest {

    private static final String TAG = "com.example.lok.intents";

    @Rule
    public final ServiceTestRule mMyServiceRule = new ServiceTestRule();

    private Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getTargetContext();
    }

    @Test(expected = NullPointerException.class)
    public void onBind() throws Exception {
        final MyService myService = new MyService();
        context.startService(new Intent(context, MyService.class));

        myService.onBind(new Intent());
    }

    @Test
    public void onStartCommand() throws Exception {
        final MyService myService = new MyService();
        context.startService(new Intent(context, MyService.class));

        assertEquals(Service.START_STICKY, myService.onStartCommand(new Intent(), 0, 0));
    }

    @Test
    public void onCreate() throws Exception {
        final MyService myService = new MyService();
        context.startService(new Intent(context, MyService.class));

        myService.onCreate();
        assertTrue(MyService.isServiceStarted());
    }

    @Test
    public void onDestroy() throws Exception {
        final MyService myService = new MyService();
        context.startService(new Intent(context, MyService.class));

        myService.onDestroy();
        assertFalse(MyService.isServiceStarted());
    }
}

