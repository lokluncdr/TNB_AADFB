package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Lok on 11/04/16.
 *
 * According to https://developer.android.com/training/testing/integration-testing/service-testing.html
 * Note: The ServiceTestRule class does not support testing of IntentService objects. If you need to test a
 * IntentService object, you should encapsulate the logic in a separate class and create a corresponding unit
 * test instead.
 *
 * Since this IntentService doesn't contain any logic, there's no real use for testing it in isolation
 * with a Unit Test...
 */
public class LuckyIntentService extends IntentService{

    //FIELDS
    private static final String TAG = "com.example.lok.intents";

    //CONSTRUCTOR
    public LuckyIntentService() {
        super("LuckyIntentService");
    }

    //METHODS
    @Override
    protected void onHandleIntent(Intent intent) {
        //This is what the service does
        Log.i(TAG, "###LuckyIntentService has isServiceStarted###");
    }
}
