package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Beach extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beach);

        Bundle waterBundle = getIntent().getExtras();
        if (waterBundle == null) {
            return;
        }

        String waterMessage = waterBundle.getString("waterMessage");
        final TextView beachTextView = (TextView) findViewById(R.id.beachTextView);
        beachTextView.setText(waterMessage);
    }

    public void goToWater(View view) {
        Intent intent = new Intent(this, Water.class);
        startActivity(intent);
    }
}
