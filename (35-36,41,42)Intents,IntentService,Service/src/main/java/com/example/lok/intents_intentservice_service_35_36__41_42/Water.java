package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

/*
Intents: Allows the user to go from one Activity (Water) to another (Beach), while passing on a
value provided in an EditText in the first Activity. Has two activities. First Activity (Water)
has a TextView, EditText and a Button. Second Activity (Beach) has a TextView and a Button. Each
Button handles the click event through a method linked through its XML's onClick attribute. The
value from Water is passed as a value in the Intent to start the Beach Activity and shown in the
TexView in the Beach Activity.

IntentService: Allows user to start a Service which runs in the background (creates it's own
thread so it doesn't interfere with the main thread) and only logs a message. In order to do this
it will be instantiated in Water's onCreate() and then isServiceStarted using startService() (after creating
the Service class of course which extends IntentService and must override onHandleIntent and adding
it in the AndroidManifest.xml).

Service: Allows user to start a Service (however it runs in the main thread/ UI thread, so it
requires that you create a working thread for executing it's work) and logs a message every 5
seconds. In order to do this it will be instantiated in Water's onCreate() and then isServiceStarted using
startService() (after creating the Service class of course which extends Service and must override
onBind() and adding it in the AndroidManifest.xml ). In onStartCommand() the code is written that
needs to be executed (Where the Thread is created and isServiceStarted).

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 35 - Intents
From https://www.youtube.com/watch?v=06bcsMx-7QQ&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=35
Android App Development for Beginners - 36 - Sending Extra Intent Data
From https://www.youtube.com/watch?v=mPGCLKRCG-8&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=36
Android App Development for Beginners - 41 - Intent Service
From https://www.youtube.com/watch?v=-sBxmjrSn34&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=41
Android App Development for Beginners - 42 - Services
From https://www.youtube.com/watch?v=9YapLOfQ-dY&index=42&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
*/

public class Water extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water);

        //Start IntentService(41)
        Intent intent = new Intent(this, LuckyIntentService.class);
        startService(intent);

        //Start Services(42)
        Intent intent2 = new Intent(this, MyService.class);
        startService(intent2);
    }

    public void goToBeach(View view) {
        Intent intent = new Intent(this, Beach.class);
        final EditText waterInput = (EditText) findViewById(R.id.editText);
        String waterMessage = waterInput.getText().toString();
        intent.putExtra("waterMessage", waterMessage);

        startActivity(intent);
    }
}
