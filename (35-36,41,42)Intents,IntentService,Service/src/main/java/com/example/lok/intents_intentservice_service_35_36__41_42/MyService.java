package com.example.lok.intents_intentservice_service_35_36__41_42;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.RestrictTo;
import android.util.Log;
/*
Services(42)

Got the code from https://medium.com/@josiassena/android-how-to-unit-test-a-service-67e5340544a5
 */
public class MyService extends Service {

    @RestrictTo(RestrictTo.Scope.TESTS)
    private static boolean isServiceStarted;

    //FIELDS
    private static final String TAG = "com.example.lok.intents";

    //CONSTRUCTOR
    public MyService() {
    }

    @Override
    public void onCreate(){
        super.onCreate();
        isServiceStarted = true;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);
        Log.i(TAG, "###onStartCommand() is called###");

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    long futureTime = System.currentTimeMillis() + 5000;
                    while (System.currentTimeMillis() < futureTime) {
                        synchronized (this) {
                            try {
                                wait(futureTime - System.currentTimeMillis());
                                Log.i(TAG, "###MyService is doing something###");
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
        };

        Thread myThread = new Thread(runnable);
        myThread.start();

        //START_STICKY -> If Service gets destroyed, then it'll be restarted again.
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
//        super.onDestroy();
        Log.i(TAG, "###onDestroy() is called###");
        isServiceStarted = false;
    }

    //METHODS
    @Override
    public IBinder onBind(Intent intent) {
//        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");

        throw null; //This is not a bound service yet.
    }

    public static boolean isServiceStarted() {
        return isServiceStarted;
    }

}
