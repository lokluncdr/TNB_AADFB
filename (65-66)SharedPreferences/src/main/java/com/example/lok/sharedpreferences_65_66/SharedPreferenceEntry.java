package com.example.lok.sharedpreferences_65_66;

/**
 * Created by Lok on 01/08/2017.
 *
 * Model class containing personal information that will be saved to SharedPreferences.
 *
 */

public class SharedPreferenceEntry {

    //FIELDS
    private String userName;
    private String passWord;

    //CONSTRUCTOR
    public SharedPreferenceEntry(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    //PROPERTIES
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
