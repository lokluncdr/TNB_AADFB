package com.example.lok.sharedpreferences_65_66;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*
Allows the user to put in two texts in 2 EditTexts and save the values in Shared Preferences on
button click "Save Info" while emptying the EditTexts, on another button click "Show Info" both
values are retrieved from Shared Preferences and shown in a TextView. Everything happens in two
methods: saveInfo() and showInfo(). In method saveInfo() a SharedPreferencesHelper object is saving
the Strings "username" and "password" with the help of an SharedPreferencesEntry object.
In showInfo() values are retrieved from the SharedPreferences with the SharedPreferencesHelper object.

Note: Refactored the code for testing purposes

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 65 - Shared Preferences
From https://www.youtube.com/watch?v=hCJkUd7YEXs&index=65&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
Android App Development for Beginners - 66 - Shared Preferences Example
From https://www.youtube.com/watch?v=xv_JJbjDQ3M&index=66&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxlFrom
 */

public class MainActivity extends AppCompatActivity {

    // Logger for this class.
    private static final String TAG = "MainActivity";

    //FIELDS
    //Input Fields
    private EditText usernameInput;
    private EditText passwordInput;

    //Output Fields
    private TextView resultText;

    // The helper that manages writing to SharedPreferences.
    private SharedPreferencesHelper mSharedPreferencesHelper;

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Shortcuts to input fields.
        usernameInput = (EditText) findViewById(R.id.editTextUsername);
        passwordInput = (EditText) findViewById(R.id.editTextPassword);

        // Shortcuts to output fields.
        resultText = (TextView) findViewById(R.id.textViewResult);

        // Instantiate a SharedPreferencesHelper.
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSharedPreferencesHelper = new SharedPreferencesHelper(sharedPreferences);
    }

    public void saveInfo(View view) {
        //        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE); //This SharedPreferences fil is only accesible to this application
        //
        //        SharedPreferences.Editor editor =  sharedPreferences.edit();
        //        editor.putString("username", usernameInput.getText().toString());
        //        editor.putString("password", passwordInput.getText().toString());
        //
        //        //Empty inputfields
        //        usernameInput.setText("");
        //        passwordInput.setText("");
        //
        //        //Write to the editor
        //        editor.apply();
        //
        //        //Notify user through Toast that text is saved
        //        Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();


        // Get the text from the input fields.
        String username = usernameInput.getText().toString();
        String password = passwordInput.getText().toString();

        //Empty inputfields
        usernameInput.setText("");
        passwordInput.setText("");

        // Create model class to persist.
        SharedPreferenceEntry sharedPreferenceEntry = new SharedPreferenceEntry(username, password);

        // Persist the personal information.
        boolean isSuccess = mSharedPreferencesHelper.savePersonalInfo(sharedPreferenceEntry);
        if (isSuccess) {
            Toast.makeText(this, "Personal information saved", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Personal information saved");
        } else {
            Log.e(TAG, "Failed to write personal information to SharedPreferences");
        }
    }

    public void showInfo(View view) {
        //        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE); //This SharedPreferences fil is only accesible to this application
        //
        //        String name = sharedPreferences.getString("username", "");
        //        String password = sharedPreferences.getString("password", "");
        //
        //        resultText.setText(name + " " + password);

        SharedPreferenceEntry sharedPreferenceEntry;
        sharedPreferenceEntry = mSharedPreferencesHelper.getPersonalInfo();
        resultText.setText(sharedPreferenceEntry.getUserName() + " " + sharedPreferenceEntry.getPassWord());
    }
}
