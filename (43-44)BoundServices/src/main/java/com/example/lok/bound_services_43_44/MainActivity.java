package com.example.lok.bound_services_43_44;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.lok.bound_services_43_44.MyService.MyLocalBinder;

/*

Allows the user to create a Bound Service in order to create communication between the Application
and a Service and thus show the current time, from Service to TextView in MainActivity. First a
Service is binded in onCreate() with an Intent, ServiceConnection, and int as parameters for the
bindService(). ServiceConnection has an anonymous inner class on object instantiation where the
Service is really being bound on object instantiation of the MyService object with a call to
getService() on the (MyLocalBinder casted) service parameter in the method onServiceConnected().
The Service itself must override onBind() where an IBinder object (used as a bridge between the
App/MainActivity (Client) and the Service) is returned. This Ibinder object is instantiated with
a custom MyLocalBinder class (object) which extends from Binder (Which implements IBinder). In
this MyLocalBinder class the Service is being returned with MyService.this in getService().

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 43 - Bound Services
From https://www.youtube.com/watch?v=0c4jRCm353c&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=43
 */

public class MainActivity extends AppCompatActivity {

    //FIELDS
    MyService myService;
    boolean isBound = false;

    //Anonynmous inner class responsible for binding to the Service
    private ServiceConnection myServiceConnection = new ServiceConnection() {
        //What needs to happen when you bind
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyLocalBinder myLocalBinder = (MyLocalBinder) service;
            myService = myLocalBinder.getService();
            isBound = true;
        }

        //What needs to happen when you disconnect
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    //METHODS
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Start Intent when Activity is created
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, myServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public void showTime(View view) {
        String currentTime = myService.getCurrentTime();

        TextView myTextView = (TextView) findViewById(R.id.myTextView);
        myTextView.setText(currentTime);
    }
}
