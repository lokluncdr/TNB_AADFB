package com.example.lok.bound_services_43_44;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyService extends Service {

    //FIELDS
    //3.
    private final IBinder myBinder = new MyLocalBinder();

    //CONSTRUCTOR
    //1.
    public MyService() {
    }

    //METHODS
    //4.
    @Override
    public IBinder onBind(Intent intent) {
//        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return myBinder;
    }

    public String getCurrentTime()
    {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        return (df.format(new Date()));
    }

    //2.
    //This binds the Client: MainActivity, and the Service: MyService
    public class MyLocalBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }
}
