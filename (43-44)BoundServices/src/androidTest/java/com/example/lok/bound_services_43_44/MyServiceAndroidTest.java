package com.example.lok.bound_services_43_44;

import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by Lok on 27/07/2017.
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class MyServiceAndroidTest {

    @Rule
    public final ServiceTestRule mServiceRule = new ServiceTestRule();

    @Test
    public void testWithBoundService() throws TimeoutException {
        //Create Service Intent
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(), MyService.class);

        //Bind Data and get a reference to the binder
        IBinder binder = mServiceRule.bindService(serviceIntent);

        //Get reference to Service, or call public methods on the binder directly
        MyService myService = ((MyService.MyLocalBinder)binder).getService();

        //Verify Service is working correctly
        String currentTime = myService.getCurrentTime();
        assertThat(currentTime, is(any(String.class)));
    }
}