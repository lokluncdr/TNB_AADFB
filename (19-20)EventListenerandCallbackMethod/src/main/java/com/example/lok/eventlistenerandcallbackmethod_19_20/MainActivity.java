package com.example.lok.eventlistenerandcallbackmethod_19_20;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/*
Allows user to change a text in a TextView when user clicks on a Button.
Shows a TextView and a Button. On button click a text is being shown and on long button click a
different text is shown in the same TextView. The button has a setOnClickListener() in which method
onClick() must be overridden when an instantiation of new Button.OnClickListener() is given as a parameter.
OnClickListener is a interface of the View class which is the parent class of Button (Button extends from TextView.
And TextView extends View class).

TNB, AADFB (The New Boston, Android App Development For Beginners)

Android App Development for Beginners - 19 - Event Listener and Callback Method
From https://www.youtube.com/watch?v=jxoG_Y6dvU8&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl&index=19
Android App Development for Beginners - 20 - Multiple Event Listeners
https://www.youtube.com/watch?v=bBUeH5oNBxI&index=20&list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //        fab.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View view) {
        //                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //                        .setAction("Action", null).show();
        //            }
        //        });


        Button btn_Main = (Button) findViewById(R.id.btn_main);

        btn_Main.setOnClickListener(new Button.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                TextView txtView_Main = (TextView) findViewById(R.id.main_txtView1);
                txtView_Main.setText(R.string.message_text);
            }
        });

        btn_Main.setOnLongClickListener(new Button.OnLongClickListener() {
            /**
             * Called when a view has been clicked and held.
             *
             * @param v The view that was clicked and held.
             * @return true if the callback consumed the long click, false otherwise.
             */
            @Override
            public boolean onLongClick(View v) {
                TextView txtView_Main = (TextView) findViewById(R.id.main_txtView1);
                txtView_Main.setText(R.string.long_message_text);

                return true;
            }
        });

        //    @Override
        //    public boolean onCreateOptionsMenu(Menu menu) {
        //        // Inflate the menu; this adds items to the action bar if it is present.
        //        getMenuInflater().inflate(R.menu.menu_main, menu);
        //        return true;
        //    }
        //
        //    @Override
        //    public boolean onOptionsItemSelected(MenuItem item) {
        //        // Handle action bar item clicks here. The action bar will
        //        // automatically handle clicks on the Home/Up button, so long
        //        // as you specify a parent activity in AndroidManifest.xml.
        //        int id = item.getItemId();
        //
        //        //noinspection SimplifiableIfStatement
        //        if (id == R.id.action_settings) {
        //            return true;
        //        }
        //
        //        return super.onOptionsItemSelected(item);
        //    }
    }
}
