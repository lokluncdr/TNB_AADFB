package com.example.lok.eventlistenerandcallbackmethod_19_20;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Lok on 18/07/2017.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityInstrumentationTest {

    //Needed so this class has access to MainActivity
    /**
     * {@link ActivityTestRule} is a JUnit {@link Rule @Rule} to launch your activity under test.
     * <p>
     * <p>
     * Rules are interceptors which are executed for each test method and are important building
     * blocks of Junit tests.
     */
    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    @Test
    public void btn_Main_OnClickListener() throws Exception {
        // Click on the "Click Here" button
        onView(withId(R.id.btn_main)).perform(click());

        //Check if text on TextView matches with right String Resource
        onView(withId(R.id.main_txtView1)).check(matches(withText(R.string.message_text)));
    }

    @Test
    public void btn_Main_OnLongClickListener() throws Exception {
        // Click on the "Click Here" button
        onView(withId(R.id.btn_main)).perform(longClick());

        //Check if text on TextView matches with right String Resource
        onView(withId(R.id.main_txtView1)).check(matches(withText(R.string.long_message_text)));
    }
}